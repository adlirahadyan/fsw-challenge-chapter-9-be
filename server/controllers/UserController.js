const { hashPassword } = require("../utils/passwordHandler");
const { User } = require("../models");
const { Op } = require("sequelize");

class UserController {
  static async register(req, res, next) {
    try {
      const { username, email, password, confirmPassword } = req.body;

      if (!username || !email) {
        return res.status(400).json({
          result: "Failed",
          message: "username or email cannot empty",
        });
      }

      if (!password) {
        return res.status(400).json({
          result: "Failed",
          message: "password cannot be empty",
        });
      }

      if (password != confirmPassword) {
        return res.status(400).json({
          result: "Failed",
          message: `password don't match`,
        });
      }

      const newUser = {
        username,
        email,
        password: await hashPassword(password),
      };

      const createdUser = await User.create(newUser);

      if (createdUser) {
        return res.status(201).json({
          result: "Register successfully!",
          data: createdUser,
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async getUsers(req, res, next) {
    try {
      let conditions = [];
      const { username, email } = req.query;
      if (username) {
        conditions.push({ username });
      }
      if (email) {
        conditions.push({ email });
      }

      const data = await User.findAll({
        where: {
          [Op.and]: conditions,
        },
      });
      if (data) {
        return res.status(200).json({
          result: "Success",
          data,
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async getUserById(req, res, next) {
    try {
      const { id } = req.params;
      const userId = await User.findByPk(id);
      if (userId) {
        return res.status(200).json({
          result: "Success",
          data: userId,
        });
      } else {
        return res.status(404).json({
          result: "Not found",
          message: `User with ${id} not found`,
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async updateUser(req, res, next) {
    try {
      const { id } = req.params;
      const userId = await User.findByPk(id);
      if (!userId)
        return res
          .status(404)
          .json({ result: "Not found", message: `User with ${id} not found` });
      const updatedUser = await User.update(req.body, {
        where: { id: id },
      });
      if (updatedUser == 1) {
        return res.status(200).json({
          result: "Success",
          message: `User with id: ${id} successfully updated`,
        });
      } else {
        return res.status(500).json({
          result: "failed",
          message: "Failed to update",
        });
      }
    } catch (error) {
      next(error);
    }
  }

  static async deleteUser(req, res, next) {
    try {
      const { id } = req.params;

      const destroyed = await User.destroy({
        where: { id: id },
      });
      if (destroyed == 1) {
        res.status(200).json({
          result: "Success",
          message: `User with id: ${id}, was deleted successfully`,
        });
      } else {
        res.status(400).json({
          result: "FAILED",
          message: `Cannot delete User with id=${id}. Maybe User was not found!`,
        });
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserController;
