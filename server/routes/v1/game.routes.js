const GameController = require('../../controllers/game.controller');
const gameRouter = require('express').Router();
const gameDetailRouter = require('./game.detail.routes');

/**
 * @Routes "/api/v1/Users"
 */

gameRouter.use('/detail', gameDetailRouter);

gameRouter.get('/', GameController.getGames);
gameRouter.post('/', GameController.createGame);
gameRouter.get('/:id', GameController.getGameById);
gameRouter.put('/:id', GameController.updateGame);
gameRouter.delete('/:id', GameController.deleteGame);

module.exports = gameRouter;
